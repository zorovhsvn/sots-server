# sots-server
Edit config overrides for production deployment:
vi config/production.json

Start your app server - production:
export NODE_ENV=production
node app

Start your app server - staging:
export NODE_ENV=staging
node app

