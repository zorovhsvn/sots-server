const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
var cors = require("cors");
const fileUpload = require("express-fileupload");
const config = require("config");
const dbConfig = config.get("dbConfig");
const glob = require("glob");
const path = require("path");
const db = mysql.createConnection(dbConfig);

db.connect(err => {
  if (err) {
    throw err;
  }

  console.log("Connect database");
});
global.db = db;

var app = express();
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileUpload());
app.set("views", __dirname + "/public");
app.set("view engine", "ejs");

app.use("/assets", express.static(__dirname + "/public"));

glob(`${__dirname}/apps/controllers/*`, {}, (err, files) => {
  if (err) {
    throw err;
  }
  if (files) {
    files.map(data => {
      let apis = data.split(path.sep).pop();
      app.use(`/api/${apis}`, require(data));
    });
  }
});

app.get("*", (req, res) => {
  res.render("index");
});

app.listen(3000, () => {
  console.log("Server is running...");
});
