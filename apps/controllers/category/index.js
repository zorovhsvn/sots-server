var express = require("express");
var router = express.Router();

const category = require('./categories')

router.post('/categories',(req,res)=> category.getListCategories(req,res))

module.exports = router;
