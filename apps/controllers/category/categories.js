const mysql = require('../../core/lib')
const table = require('../../core/table')
const getListCategories = async(req,res)=>{
    let {params,fields} = req.body;
    let categories = await mysql.select(table.TABLE_CATEGORY,Object.assign(params,{disable:"active"}),fields,res)
    if(categories){
        res.json({stt:1,data:categories,msg:"Get Product success",success:true})
    }
}

module.exports = {
    getListCategories
}