var express = require("express");
var router = express.Router();

const products = require('./products')

router.post('/products',(req,res)=> products.getListProduct(req,res))

router.post('/topselled',(req,res)=>products.getTopProductSelled(req,res))

router.post('/new',(req,res)=>products.getNewProducts(req,res))

router.post('/detail',(req,res)=>products.getProductDetail(req,res))
module.exports = router;
