const mysql = require("../../core/lib");
const table = require("../../core/table");

const getListProduct = async (req, res) => {
  let { params, fields } = req.body;

  let product = await mysql.select(table.TABLE_PRODUCTS, params, fields, res);
  if (product) {
    res.json({
      stt: 1,
      data: product,
      msg: "Get Product success",
      success: true
    });
  }
};

const getProductDetail = async (req, res) => {
  let { params, fields } = req.body;
  let success;
  let productDetail = await mysql.select(
    table.TABLE_PRODUCTS,
    params,
    fields,
    res
  );

  if (productDetail) {
    if (productDetail.length > 0) {
      success = {
        data: productDetail[0]
      };
    } else {
      success = { data: [] };
    }
    res.json(
      Object.assign(success, {
        stt: 1,
        msg: "Get Product success",
        success: true
      })
    );
  }
};

const getTopProductSelled = async (req, res) => {
  let { fields } = req.body;

  let product = await mysql.select(
    table.TABLE_PRODUCTS,
    { limit: 10 },
    fields,
    res,
    { fields: "selled", type: "DESC" }
  );
  if (product) {
    res.json({
      stt: 1,
      data: product,
      msg: "Get Product success",
      success: true
    });
  }
};

const getNewProducts = async (req, res) => {
  let { fields } = req.body;

  let product = await mysql.select(
    table.TABLE_PRODUCTS,
    { limit: 10 },
    fields,
    res,
    { fields: "time", type: "DESC" }
  );
  if (product) {
    res.json({
      stt: 1,
      data: product,
      msg: "Get Product success",
      success: true
    });
  }
};

module.exports = {
  getListProduct,
  getTopProductSelled,
  getNewProducts,
  getProductDetail
};
