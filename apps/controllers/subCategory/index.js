var express = require("express");
var router = express.Router();

const subCategory = require('./subCategory')

router.post('/subcategory',(req,res)=> subCategory.getListSubCategories(req,res))

module.exports = router;
