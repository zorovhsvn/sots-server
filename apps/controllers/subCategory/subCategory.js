const mysql = require('../../core/lib')
const table = require('../../core/table')
const getListSubCategories = async(req,res)=>{
    let {params,fields} = req.body;
    if(params.id){
        let subCategory = await mysql.select(table.TABLE_SUBCATEGORY,params,fields,res)
        res.json({stt:1,data:subCategory,msg:"Get sub category success",success:true})
    }else{
        res.json({stt:0,data:subCategory,msg:"Get sub category fail",success:true})
    }

}

module.exports = {
    getListSubCategories
}