const mysql = require("../../core/lib");
const table = require("../../core/table");
const getListSliders = async (req, res) => {
  let { params, fields } = req.body;

  let sliders = await mysql.select(table.TABLE_SLIDER, Object.assign(params,{disable:"active"}), fields, res);
  if (sliders) {
    res.json({
      stt: 1,
      data: sliders,
      msg: "Get Product success",
      success: true
    });
  }
};

module.exports = {
    getListSliders
};
