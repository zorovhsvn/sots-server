var express = require("express");
var router = express.Router();

const sliders = require('./sliders')

router.post('/sliders',(req,res)=> sliders.getListSliders(req,res))

module.exports = router;
