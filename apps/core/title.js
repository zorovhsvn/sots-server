

const UPDATE_SUCCESS = "Thay đổi thành công";
const UPDATE_FAILED  = "Thay đổi thất bại";
const ADD_SUCCESS = "Thêm thành công";
const ADD_FAILED = "Thêm thất bại";
const DELETE_SUCCESS = "Xóa thành công";
const DELETE_FAILED = "Xóa thất bại";
const NOT_FOUND = "Không tìm thấy "
const NOT_ADMIN = "Không phải admin";
const INPUT_EMTRY = "Vui lòng nhập đủ thông tin!"
const FAVORITE_SUCCESS = "Yêu thích thành công"
const FAVORITE_UNLIKE_SUCCESS = "Hủy yêu thích thành công"
const FAVORITE_LIST_EMPTY = "Danh sách yêu thích trống vui lòng thử lại sau"

module.exports = {
    UPDATE_FAILED,
    UPDATE_SUCCESS,
    ADD_FAILED,
    ADD_SUCCESS,
    DELETE_SUCCESS,
    DELETE_FAILED,
    NOT_FOUND,
    NOT_ADMIN,
    INPUT_EMTRY,
    FAVORITE_SUCCESS,
    FAVORITE_LIST_EMPTY,
    FAVORITE_UNLIKE_SUCCESS
}
