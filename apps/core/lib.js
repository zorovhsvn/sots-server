let jwt = require("jsonwebtoken");
const config = require("./config.js");

checkToken = (req, callback) => {
  let token = req.headers["x-access-token"] || req.headers["authorization"]; // Express headers are auto converted to lowercase
  let err, success;

  if (token) {
    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length);
      jwt.verify(token, config.privateKey, (error, data) => {
        if (error) {
          err = {
            stt: 401,
            success: false,
            msg: "Authentication failed! Please check the request"
          };
        } else {
          success = data;
        }
      });
    }
  } else {
    err = {
      stt: 401,
      success: false,
      msg: "Authentication Emtry! Please check the request"
    };
  }

  callback(err, success);
};

query = async (query) => {
  return await new Promise((resolve, reject) => {
    db.query(query, (err, result) => {
      if (!err) {
        resolve(result);
      } else {
        reject(err);
      }
    });
  });
};

insert = async (table, data) => {
  let column = "";
  let value = "";
  if (data != undefined && data != null) {
    Object.keys(data).map((keys, i) => {
      if (data[keys] != "" && data[keys] != null && data[keys] != undefined) {
        column += `${Object.keys(data)[i]}${
          i != Object.keys(data).length - 1 ? "," : ""
        }`;
        value += `'${data[keys]}'${
          i != Object.keys(data).length - 1 ? "," : ""
        }`;
      }
    });
    let querys = `INSERT INTO ${table}(${column})VALUES(${value})`;
    return query(querys);
  }
};

select = async (table, data, fields, res, sort) => {
  if (data != undefined && data != null) {
    let where = `id != 0`;
    let limit,
      page = 0;
    let sorts = sort ? `${sort.fields} ${sort.type}`:`id ASC `;

    if (data.limit) {
      limit = data.limit;
    } else {
      limit = 10;
    }
    if (data.page && data.page > 1) {
      page = data.page;
    } else {
      page = 1;
    }
    let skip = (page - 1) * limit;

    if (data != undefined && Object.keys(data).length > 0) {
      Object.keys(data).map((keys, i) => {
        if (
          data[keys] != "" &&
          data[keys] != null &&
          data[keys] != undefined &&
          Object.keys(data)[i] != "limit" &&
          Object.keys(data)[i] != "page"
        ) {
        where += ` AND ${Object.keys(data)[i]} = '${data[keys]}'`;
        }
      });
    }

    let querys = `SELECT ${
      fields == undefined || fields == null || fields == "" ? "*" : fields
    } FROM ${table} where ${where} ORDER BY ${sorts} LIMIT ${skip},${limit} `;

    return query(querys);
  } else {
    res.json({ stt: 400, msg: "Syntax Error: Expected params,fields" });
  }
};

update = async (table, data, dataUpdate, res) => {

  if (data != undefined && data != null) {
    let { limit, page } = data;
    let where = "";
    if (!limit) limit = 10;
    if (!page) page = 1;
    let skip = (page - 1) * limit;
    let fieldsUpdate = "";

    if (Object.keys(data).length > 0) {
      Object.keys(data).map((keys, i) => {
        if (data[keys] != "" && data[keys] != null && data[keys] != undefined) {
          if (i == 0) {
            where += `${Object.keys(data)[i]} = '${data[keys]}'`;
          } else {
            where += ` AND ${Object.keys(data)[i]} = '${data[keys]}'`;
          }
        }
      });
    }

    if (Object.keys(dataUpdate).length > 0) {
      Object.keys(dataUpdate).map((keys, i) => {
        if (
          dataUpdate[keys] != "" &&
          dataUpdate[keys] != null &&
          dataUpdate[keys] != undefined
        ) {
          if (i < Object.keys(dataUpdate).length - 1) {
            fieldsUpdate += `${Object.keys(dataUpdate)[i]}='${
              dataUpdate[keys]
            }',`;
          } else {
            fieldsUpdate += `${Object.keys(dataUpdate)[i]}='${
              dataUpdate[keys]
            }'`;
          }
        } else {
          fieldsUpdate += `${Object.keys(dataUpdate)[i]}='${dataUpdate[keys]}'`;
        }
      });
    }

    let querys = `UPDATE ${table} SET ${fieldsUpdate} where ${where}`;

    return query(querys);
  } else {
    res.json({ stt: 400, msg: "Syntax Error: Expected fields" });
  }
};

module.exports = {
  query,
  select,
  update,
  insert,
  checkToken
};
